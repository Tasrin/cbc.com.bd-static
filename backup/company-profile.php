<?php 
  include('header.php');
?>
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Company Profile</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Company Profile <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-5 p-md-5 img img-2 mt-5 mt-md-0" style="background-image: url(images/bg_2.jpg);">
					</div>
					<div class="col-md-7 wrap-about py-5 px-4 px-md-5 ftco-animate">
	          <div class="heading-section mb-5">
	            <h2 class="mb-4">Our Proifile</h2>
	          </div>
	          <div class="table-responsive">
							<table class="table table-bordered table-dark">
                <thead>
                  <tr>
                    <th scope="col">Equipement Name</th>
                    <th scope="col">Measurment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Establishment Year</td>
                    <td>1985</td>
                  </tr>
                  <tr>
                    <td>Adviser</td>
                    <td>1 Person</td>
                  </tr>
                  <tr>
                    <td>Man Power</td>
                    <td>103 nos <br/><small>(Except worker supplied to other factory)</small></td>
                  </tr>
                  <tr>
                    <td>Engineers (Civil & Mechanical)</td>
                    <td>08 nos (B.Sc & Diploma)</td>
                  </tr>
                  <tr>
                    <td>Designer Cum Cad Operator</td>
                    <td>03 nos</td>
                  </tr>
                  <tr>
                    <td>Supervisors</td>
                    <td>15 nos</td>
                  </tr>
                  <tr>
                    <td>Health & Safety Consultant</td>
                    <td>01 nos</td>
                  </tr>
                  <tr>
                    <td colspan="2" class="text-left"><strong>Equipement</strong></td>
                  </tr>
                  <tr>
                    <td>Crane_Heavy @ 20 tons capacity</td>
                    <td>02 pcs</td>
                  </tr>
                  <tr>
                    <td>Light @ 5 tons capacity</td>
                    <td>04 pcs</td>
                  </tr>
                  <tr>
                    <td>Scaffolding: 40' height</td>
                    <td>100 pcs</td>
                  </tr>
                  <tr>
                    <td>Steel High Stair</td>
                    <td>10 pcs</td>
                  </tr>
                  <tr>
                    <td>Rod Cutter</td>
                    <td>04 pcs</td>
                  </tr>
                  <tr>
                    <td>Steel Sheet Cutter</td>
                    <td>06 pcs</td>
                  </tr>
                  <tr>
                    <td>Cable Punching Tools in different rm</td>
                    <td>04 pcs</td>
                  </tr>
                  <tr>
                    <td>DC Welding Machine</td>
                    <td>10 nos</td>
                  </tr>
                  <tr>
                    <td>AC Welding Machine</td>
                    <td>06 nos</td>
                  </tr>
                  <tr>
                    <td>Argon Welding Machine</td>
                    <td>06 nos</td>
                  </tr>
                  <tr>
                    <td>Full Workshop Machinery</td>
                    <td>04 net</td>
                  </tr>
                  <tr>
                    <td>Mini Truck (3 ton capacity)</td>
                    <td>03 nos</td>
                  </tr>
                  <tr>
                    <td>And all other required equipment</td>
                    <td>01 lot</td>
                  </tr>
                </tbody>
              </table>
						</div>
					</div>
				</div>
			</div>
		</section>
    
   <?php 
    include('footer.php');
  ?>