<?php 
	get_header();
?>
	
<div class="single">
	<div class="container">
		
		<div class="wrap-about py-5 ftco-animate">
	        <div class="heading-section mb-5">
	            <h2 class="mb-4 text-center">404 NOT FOUND!</h2>
	        </div>
	        <div class="">
	        	
				<p class="text-justify text-center">
					Maybe you are looking for searchimg else!<br/>
					Please visit<a href="<php bloginfo('home');?>">home page</a>.
				
				</p>
			</div>
		</div>

	</div>
</div>

<?php 
	get_footer();
?>