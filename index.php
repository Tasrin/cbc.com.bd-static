<?php 
	get_header();
?>
    
    <section class="home-slider owl-carousel">

	<?php
		$slider = new WP_Query(array(
			'post_type'=>'post',
			'category_name'=>'slider',
			'posts_per_page'=>2,
			'order'=>'ASC',
		));
	?>
    <?php while($slider->have_posts()): $slider->the_post();?>

    	<div class="slider-item" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID,'ExternalUrl',true),'thumbnail');?>);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
          <div class="col-md-6 text ftco-animate pl-md-5">
            <h1 class="mb-4"><?php the_title();?></h1>
            <h3 class="subheading"><?php the_content();?></h3>
            <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Request A Quote</a></p>
          </div>
        </div>
        </div>
      </div>
  <?php endwhile; ?>
    <!--  <div class="slider-item" style="background-image:url(<?php// echo esc_url(get_template_directory_uri());?>/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
          <div class="col-md-6 text ftco-animate pl-md-5">
            <h1 class="mb-4">Base Construction <span>Build The Future</span></h1>
            <h3 class="subheading">The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh.</h3>
            <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Request A Quote</a></p>
          </div>
        </div>
        </div>
      </div>

      <div class="slider-item" style="background-image:url(<?php //echo esc_url(get_template_directory_uri());?>/images/bg_2.jpg);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
          <div class="col-md-6 text ftco-animate pl-md-5">
            <h1 class="mb-4">We Turn Your <span>Vision Into Reality</span></h1>
            <h3 class="subheading">The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh.</h3>
            <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Request A Quote</a></p>
          </div>
        </div>
        </div>
      </div>-->



    </section>


    <section class="ftco-section ftco-no-pt ftco-margin-top">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-4">
    				<div class="request-quote">
    					<!--<div class="bg-primary py-4">
    						<span class="subheading">Be Part of our Business</span>
    						<h3>Request A Quote</h3>
    					</div>-->
    					<form action="#" class="request-form ftco-animate">
		    				<div class="form-group">
		    					<input type="text" class="form-control" placeholder="First Name">
		    				</div>
		    				<div class="form-group">
		    					<input type="text" class="form-control" placeholder="Last Name">
		    				</div>
	    					<div class="form-group">
		    					<div class="form-field">
          					<div class="select-wrap">
                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                      <select name="" id="" class="form-control">
                      	<option value="">Select Your Services</option>
                        <option value="">Construction</option>
                        <option value="">Renovation</option>
                        <option value="">Interior Design</option>
                        <option value="">Exterior Design</option>
                        <option value="">Painting</option>
                      </select>
                    </div>
		              </div>
		    				</div>
	    					<div class="form-group">
		    					<input type="text" class="form-control" placeholder="Phone">
		    				</div>
	    					<div class="form-group">
		              <textarea name="" id="" cols="30" rows="2" class="form-control" placeholder="Message"></textarea>
		            </div>
		            <div class="form-group">
		              <input type="submit" value="Appointment" class="btn btn-primary py-3 px-4">
		            </div>
		    			</form>
    				</div>
    			</div>
    			<div class="col-md-8 wrap-about py-5 ftco-animate">
	          <div class="heading-section mb-5">
	            <h2 class="mb-4">We Are Highly Recommendable Construction Firm</h2>
	          </div>
	          <div class="">
							<p class="text-justify">The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh. The organization established in
			                the year 1990 considering its aim & objects as follows. We have been
			                working successfully with good reputation dialed with more than 200
			                companies in Chattogram EPZ, KEPZ, Korean EPZ and other government,
			                semi-government, autonomous etc. factories and organization in
			                industrial sector.<br/><br/> Our goal to expand our services widely to the industrial
			                sector in Bangladesh with modern effective evaluation.
			                We are the authorized contractor of BEPZA.</p>
							<p><a href="mission-vission.php" class="btn btn-secondary px-5 py-3">Our Mission & Vission</a></p>
						</div>
					</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-services ftco-no-pt">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
          	<span class="subheading">Services</span>
            <h2 class="mb-4">Our Services</h2>
            <p>The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh.</p>
          </div>
        </div>
		<div class="row">


         <?php 
			$servpost = new WP_Query(array(
				'post_type'		=>'post',
				'category_name'	=>'services',
				'order'			=>'DESC',
				'post_per_page'	=> 4,
			));		
		?>	

	
		<?php while($servpost->have_posts()) : $servpost->the_post(); ?>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate hsrv">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
            		<?php the_post_thumbnail(); ?>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading"><a href="<?php the_permalink(); ?>">
                	<?php the_title(); ?></a></h3>
                <p><?php read_more(13); ?><a href="<?php the_permalink(); ?>">...Read More</a></p>
              </div>
            </div>      
          </div>
      	<?php endwhile; ?>



			


<!--
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
            		<span class="flaticon-skyline"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Industrial Fabrication</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
            		<span class="flaticon-stairs"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Mechanical Works</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
            		<span class="flaticon-home"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Repair and Restoration</h3>
               <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>-->
        </div>
			</div>
		</section>
		
		
		<!--<section class="ftco-intro" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
						<h2>Lets Build Your Dream Together</h2>
						<p>We can manage your dream building A small river named Duden flows by their place</p>
						<p class="mb-0"><a href="#" class="btn btn-primary px-4 py-3">Know more about us</a></p>
					</div>
				</div>
			</div>
		</section>
		
		<section class="ftco-section">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
          	<span class="subheading">Team</span>
            <h2 class="mb-4">Our Professional Team</h2>
            <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
          </div>
        </div>	
				<div class="row">
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/team-1.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3>Daren Wilson</h3>
								<span class="position mb-2">Head Engineer</span>
								<div class="faded">
									
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/team-2.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3>Warren Parker</h3>
								<span class="position mb-2">Ass. Engineer</span>
								<div class="faded">
									
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/team-3.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3>Eva Gustavo</h3>
								<span class="position mb-2">Engineer</span>
								<div class="faded">
									
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/team-4.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3>Mike Henderson</h3>
								<span class="position mb-2">Architect</span>
								<div class="faded">
									
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>-->

		<section class="ftco-section ftco-no-pt ftco-no-pb">
			<div class="container-fluid p-0">
				<div class="row no-gutters justify-content-center mb-5 pb-2">
          <div class="col-md-6 text-center heading-section ftco-animate">
          	<span class="subheading">Projects</span>
            <h2 class="mb-4">Featured Projects</h2>
            <p>we determine to expand our services widely to the Industrial sector in Bangladesh with modern effective evaluation</p>
          </div>
        </div>
    		<div class="row no-gutters">
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-1.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-2.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-2.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-3.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-3.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-4.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-4.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-5.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-5.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-6.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Resedencial</span>
	    					<h3><a href="project.html">Rose Villa House</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-6.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-7.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-7.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="project">
	    				<img src="<?php echo esc_url(get_template_directory_uri());?>/images/work-8.jpg" class="img-fluid" alt="Colorlib Template">
	    				<div class="text">
	    					<span>Commercial</span>
	    					<h3><a href="project.html">San Francisco Tower</a></h3>
	    				</div>
	    				<a href="<?php echo esc_url(get_template_directory_uri());?>/images/work-8.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
	    					<span class="icon-expand"></span>
	    				</a>
    				</div>
    			</div>
    		</div>
    	</div>
		</section>
    <section class="ftco-counter img" id="section-counter" style="background-image: url(<?php echo esc_url(get_template_directory_uri());?>/images/bg_3.jpg);" data-stellar-background-ratio="0.5">
    	<div class="container">
				<div class="row">
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 d-flex">
              <div class="text d-flex align-items-center">
                <strong class="number" data-number="30">0</strong>
              </div>
              <div class="text-2">
              	<span>Years of <br>Experienced</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 d-flex">
              <div class="text d-flex align-items-center">
                <strong class="number" data-number="1500">0</strong>
              </div>
              <div class="text-2">
              	<span>Project <br>Successful</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 d-flex">
              <div class="text d-flex align-items-center">
                <strong class="number" data-number="100">0</strong>
              </div>
              <div class="text-2">
              	<span>Professional <br>Expert</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 d-flex">
              <div class="text d-flex align-items-center">
                <strong class="number" data-number="300">0</strong>
              </div>
              <div class="text-2">
              	<span>Happy <br>Customers</span>
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    <!--<section class="ftco-section testimony-section">
      <div class="container">
        <div class="row ftco-animate">
        	<div class="col-md-6 col-lg-6 col-xl-4">
        		<div class="heading-section ftco-animate">
	          	<span class="subheading">Services</span>
	            <h2 class="mb-4">Experience Great Services</h2>
	          </div>
        		<div class="services-flow">
        			<div class="services-2 p-4 d-flex ftco-animate">
        				<div class="icon">
        					<span class="flaticon-engineer"></span>
        				</div>
        				<div class="text">
	        				<h3>Expert &amp; Professional</h3>
	        				<p>Separated they live in. A small river named Duden flows</p>
        				</div>
        			</div>
        			<div class="services-2 p-4 d-flex ftco-animate">
        				<div class="icon">
        					<span class="flaticon-engineer-1"></span>
        				</div>
        				<div class="text">
	        				<h3>High Quality Work</h3>
	        				<p>Separated they live in. A small river named Duden flows</p>
	        			</div>
        			</div>
        			<div class="services-2 p-4 d-flex ftco-animate">
        				<div class="icon">
        					<span class="flaticon-engineer-2"></span>
        				</div>
        				<div class="text">
	        				<h3>24/7 Help Support</h3>
	        				<p>Separated they live in. A small river named Duden flows</p>
	        			</div>
        			</div>
        		</div>
        	</div>
        	<div class="col-xl-1 d-xl-block d-none"></div>
          <div class="col-md-6 col-lg-6 col-xl-7">
          	<div class="heading-section ftco-animate mb-5">
	          	<span class="subheading">Testimonials</span>
	            <h2 class="mb-4">Satisfied Customer</h2>
	            <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
	          </div>
            <div class="carousel-testimony owl-carousel">
              <div class="item">
                <div class="testimony-wrap">
                  <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Racky Henderson</p>
                    <span class="position">Farmer</span>
                  </div>
                  <div class="user-img" style="background-image: url(images/person_1.jpg)">
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap">
                  <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Henry Dee</p>
                    <span class="position">Businessman</span>
                  </div>
                  <div class="user-img" style="background-image: url(images/person_2.jpg)">
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap">
                  <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Mark Huff</p>
                    <span class="position">Students</span>
                  </div>
                  <div class="user-img" style="background-image: url(images/person_3.jpg)">
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap">
                  <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Rodel Golez</p>
                    <span class="position">Striper</span>
                  </div>
                  <div class="user-img" style="background-image: url(images/person_4.jpg)">
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap">
                  <div class="text bg-light p-4">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Ken Bosh</p>
                    <span class="position">Manager</span>
                  </div>
                  <div class="user-img" style="background-image: url(images/person_1.jpg)">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


		<section class="ftco-section bg-light">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
          	<span class="subheading">Blog</span>
            <h2 class="mb-4">Recent Blog</h2>
            <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
          </div>
        </div>
				<div class="row">
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
								<div class="meta-date text-center p-2">
                  <span class="day">07</span>
                  <span class="mos">February</span>
                  <span class="yr">2019</span>
                </div>
              </a>
              <div class="text pt-4">
                <h3 class="heading"><a href="#">Office of the Florida</a></h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <div class="d-flex align-items-center mt-4">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
								<div class="meta-date text-center p-2">
                  <span class="day">07</span>
                  <span class="mos">February</span>
                  <span class="yr">2019</span>
                </div>
              </a>
              <div class="text pt-4">
                <h3 class="heading"><a href="#">Office of the Florida</a></h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <div class="d-flex align-items-center mt-4">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
								<div class="meta-date text-center p-2">
                  <span class="day">07</span>
                  <span class="mos">February</span>
                  <span class="yr">2019</span>
                </div>
              </a>
              <div class="text pt-4">
                <h3 class="heading"><a href="#">Office of the Florida</a></h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <div class="d-flex align-items-center mt-4">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</section>-->

		
   <?php 
		get_footer();
	?>